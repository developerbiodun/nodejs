const Reports = require("../model/Reports");
const colors = require("colors");

/**
 * Get Single Report
 *
 */

exports.getAllReport = async (req, res, next) => {
  try {
    let query;
    let getReport;

    // Copy of req.query
    const reqQuery = { ...req.query };

    // Remove these keywords/ Fields from the query string
    let removeFields = ["page", "limit", "companyId"];

    // Loop over the fields/ Keyword and remove it from the query string
    removeFields.forEach((item) => delete reqQuery[item]);

    // Create a query String
    let queryStr = JSON.stringify(reqQuery);

    // Pagination
    const page = parseInt(req.query.page, 10) || 1;
    const limit = parseInt(req.query.limit, 10) || 2;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const totalPage = await Reports.countDocuments();

    query = Reports.find(JSON.parse(queryStr));

    // To query for companyId

    if (req.query.companyId) {
      getReport = await query.where({ company: req.query.companyId });
    } else {
      getReport = await query.skip(startIndex).limit(limit).populate("company");
    }

    // Pagination with next and prev
    let pagination = {};

    if (endIndex < totalPage) {
      pagination.next = {
        page: page + 1,
        limit,
      };
    }

    if (startIndex > 0) {
      pagination.prev = {
        page: page - 1,
        limit,
      };
    }

    return res.status(200).json({
      success: "true",
      count: getReport.length,
      pagination,
      data: getReport,
    });
  } catch (error) {
    res.status(404).json({
      msg: `Not Found! with ${req.params.id}`,
      error: error.message,
    });
  }
};

/**
 * POST Create Reports
 *
 */

exports.createReport = async (req, res, next) => {
  try {
    const report = await Reports.create(req.body);

    res.status(201).json({
      msg: "Successful",
      report,
    });
  } catch (error) {
    res.status(400).json({
      error: error.message,
    });
  }
};
