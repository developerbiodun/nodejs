const Companies = require("../model/Companies");

/**
 * Get Single Company listing
 *
 */

exports.getSingleCompany = async (req, res, next) => {
  try {
    const getCompanyById = await Companies.findById(req.params.id).populate(
      "report"
    );

    if (!getCompanyById) {
      return res.status(400).json({
        msg: "Invalid ID - Not Found!",
      });
    }

    res.status(200).json({
      msg: "Completed",
      data: getCompanyById,
    });
  } catch (error) {
    res.status(404).json({
      msg: `Page - Not Found!`,
    });
  }
};

/**
 * Get find all Company information
 *
 */

exports.getAllCompany = async (req, res, next) => {
  try {
    const getCompany = await Companies.find().populate("report");

    res.status(200).json({
      msg: "Completed",
      data: getCompany,
    });
  } catch (error) {
    res.status(404).json({
      msg: `Not Found!`,
    });
  }
};

/**
 * POST Create Companies Information
 *
 */

exports.createCompany = async (req, res, next) => {
  try {
    const companies = await Companies.create(req.body);

    res.status(201).json({
      msg: "Created Succesful",
      data: companies,
    });
  } catch (error) {
    res.status(400).json({
      error: error.message,
    });
  }
};
