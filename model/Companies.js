const mongoose = require("mongoose");

const companySchema = new mongoose.Schema(
  {
    name: {
      unique: true,
      type: String,
      required: [true, "Please add a name value"],
      trim: true,
    },
    address: {
      type: String,
      required: [true, "Please add an address value"],
      trim: true,
    },
    email: {
      type: String,
      required: [true, "Please add  a email value"],
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        "Please add a valid email",
      ],
      trim: true,
    },
    description: {
      type: String,
      required: [true, "Please add description value"],
      trim: true,
    },
    reports: {
      type: [Number],
      enum: [1, 2, 10],
      required: true,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

//Reverse populate with virtual

companySchema.virtual("report", {
  ref: "Reports",
  localField: "_id",
  foreignField: "company",
  justOne: false,
});

module.exports = mongoose.model("Companies", companySchema);
