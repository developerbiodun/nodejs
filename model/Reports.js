const mongoose = require("mongoose");

const reportSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
  },
  type: {
    type: String,
    trim: true,
  },
  period: {
    type: String,
    maxlength: [2, "Must be 2 character sets, eg. Q3"],
    trim: true,
  },
  year: {
    type: Number,
    maxlength: [4, "Must be 4 character sets, eg. 2001"],
    required: true,
  },
  assignee: {
    type: String,
    trim: true,
  },

  deadline: {
    type: Date,
    default: Date.now,
  },

  submitted: {
    type: Boolean,
  },

  url: {
    type: String,
    trim: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  company: {
    type: mongoose.Schema.ObjectId,
    ref: "Companies",
    required: true,
  },
});

module.exports = mongoose.model("Reports", reportSchema);
