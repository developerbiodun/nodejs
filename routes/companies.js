const express = require("express");
const router = express.Router();
const {
  getSingleCompany,
  createCompany,
  getAllCompany,
} = require("../controllers/companies");

// To get Single Entry
router.route("/:id").get(getSingleCompany);

//Re-route into other router

// Creating Post
router.route("/").post(createCompany).get(getAllCompany);

module.exports = router;
