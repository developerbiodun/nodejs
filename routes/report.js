const express = require("express");
const router = express.Router();
const { getAllReport, createReport } = require("../controllers/report");
const companyRouter = require("../model/Companies");

// To get and craete reports
router.route("/").get(getAllReport).post(createReport);

module.exports = router;
