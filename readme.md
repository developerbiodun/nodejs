# NodeJS API

> 
Backend API helps in the Creation, Modification, Deletion of an organization data directory which is built on MongoDB. There are two Models/ Collections using advanced MongoDB features like virtual collection, and also Open API Specification


Once running kindly use this path for the Open API Specification    **/api-docs**

## Install Dependencies

```
npm install

```

## Run App

```
# Run in dev mode
npm start

once running kindly use this path for the Open API Specification    /api-docs
```

## **Database Seeder**

To seed the database with company, report, with data from the "\_data" folder, run

```
# Destroy all data
node seeder -d

# Import all data
node seeder -i
```
