const fs = require("fs");
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const color = require("colors");

// Load the config file
dotenv.config({ path: "./config/config.env" });

//Load the Models
const Companies = require("./model/Companies");
const Report = require("./model/Reports");

// connect to DB
mongoose.connect(process.env.DB_URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
});

//Load the JSON files

const company = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/companies.json`, "utf-8")
);

const report = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/reports.json`, "utf-8")
);

// Import into the DB
const importData = async () => {
  try {
    await Companies.create(company);
    await Report.create(report);
    console.log("Data Imported".green.inverse);
    process.exit();
  } catch (err) {
    console.error(err);
  }
};

// Delete ALL from DB

const deleteData = async () => {
  try {
    await Companies.deleteMany();
    await Report.deleteMany();
    console.log("Data Deleted".red.inverse);
    process.exit();
  } catch (err) {
    console.error(err);
  }
};

if (process.argv[2] === "-i") {
  importData();
} else if (process.argv[2] === "-d") {
  deleteData();
}
