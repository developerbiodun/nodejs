const app = require("./app");
const connectDb = require("./config/db");
const colors = require("colors");

const server = app.listen(
  process.env.PORT,
  console.log(
    `Server starting on a ${process.env.NODE_ENV} mode on port ${process.env.PORT}`
      .inverse
  )
);

// DB Connection
connectDb();

// Handling DB expections and teardown the application happen occurred.

process.on("unhandledRejection", (err, promise) => {
  console.log(`Error: ${err.message}`);

  //Close the node app

  server.close(() => process.exit(1));
});
