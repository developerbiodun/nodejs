/**
 * @jest-envirnoment node
 */

const supertest = require("supertest");
const app = require("../app");
const request = supertest(app);

describe("Company", () => {
  test("Should create company", async () => {
    const response = await request.post("/companies").send({
      name: "Stear2 Business Ltd",
      address: "9a Sir Samuel Manuwa, Victoria Island, Lagos",
      email: "salktome@stearsng.com",
      description:
        "Stears Business, our publishing arm, provides business news analysis and insight through its network of journalists and professionals in banking, consulting, law, academia, government and civil society. Our Writer’s Network includes writers based in Nigeria, Canada, United States and the United Kingdom.",
      reports: [1, 2, 10],
    });

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("pass!");
  });
});
