const mongoose = require("mongoose");

const dbConnection = async () => {
  const conn = await mongoose.connect(process.env.DB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  });
  console.log(`Mongo Database is conected ${conn.connection.host}`);
};

module.exports = dbConnection;
